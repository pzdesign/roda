<?php
/**
 * Created by PhpStorm.
 * User: flashk
 * Date: 10.09.2016
 * Time: 15:52
 */

namespace ColorModule;

use Nette;
use Nette\Database\Context;
use WebLoader\Compiler;
use WebLoader\FileCollection;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

abstract class BasePresenter extends \MainPresenter\MainPresenter
{

    /** @var Nette\Database\Context */
    private $database;

    /**
     * Továrnička pro komponentu načítající CSS.
     * @return CssLoader
     */
    public function createComponentCss() {
        // připravíme seznam souborů
        // FileCollection v konstruktoru může dostat výchozí adresář, pak není potřeba psát absolutní cesty
        $files = new FileCollection(WWW_DIR . '/css');

        // bootstrap
        $files->addFile('bootstrap.min.css');


        // custom style
        $files->addFile('color.css');

		// custom style
		$files->addFile('owl.carousel.css');
		$files->addFile('owl.theme.css');

        // kompilátoru seznam předáme a určíme adresář, kam má kompilovat
        $compiler = Compiler::createCssCompiler($files, WWW_DIR . '/webtemp');

        $compiler->addFilter(function ($code) {
            return \CssMin::minify($code);
        });
        // nette komponenta pro výpis <link>ů přijímá kompilátor a cestu k adresáři na webu
        return new CssLoader($compiler, $this->template->basePath . '/webtemp');
    }

    /**
     * Továrnička pro komponentu načítající JS.
     * @return JavaScriptLoader
     */
    public function createComponentJs() {
        $files = new FileCollection(WWW_DIR . '/js');
        // můžeme načíst i externí js
        //$files->addRemoteFile('http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js');
        //jQuery
        $files->addFile('jquery-3.1.0.min.js');
		//Owl carousel
		$files->addFile('owl.carousel.min.js');
        //jQuery UI
        $files->addFile('bootstrap.min.js');

		//Nette ajax
		$files->addFile('nette.ajax.js');


        //Defaultni JS
        $files->addFile('init.js');


        $compiler = Compiler::createJsCompiler($files, WWW_DIR . '/webtemp');

        //minifikace
        $compiler->addFilter(function ($code) {
            return \JSMin::minify($code);
        });

        return new JavaScriptLoader($compiler, $this->template->basePath . '/webtemp');
    }

}