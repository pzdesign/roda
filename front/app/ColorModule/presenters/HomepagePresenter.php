<?php

namespace ColorModule;

use Components\ColorsControl;
use Models\ColorsModel;
use Nette\Database\Context;
use Nette\Http\Session;
use Tracy\Debugger;


class HomepagePresenter extends \ColorModule\BasePresenter
{

	const EXTERIOR = 0;
	const INTERIOR = 1;

	/** @var \Models\ColorsModel */
	private $colorsModel;

	/** @var  Context $database
	 * */
	private $database;

	/** @var \Nette\Http\SessionSection */
	private $session;

	public $table;

	public $act;

	public $color;

	public function __construct(Context $database, Session $session)
	{
		// a získáme přístup do sekce 'mySection':
		$this->session = $session->getSection('use');
		$this->session = $session->getSection('preview');

		$this->database = $database;
		$this->session->preview = 0;
	}

	/**
	 * DI
	 * @param ColorsModel $colorsModel
	 * @internal param ColorModel $colorModel
	 */
	public function injectModel(ColorsModel $colorsModel)
	{
		$this->colorsModel = $colorsModel;
	}

	public function beforeRender()
	{
		$this->template->cc2d = $colors = $this->colorsModel->getColors2d();
		$this->template->cc3d = $colors = $this->colorsModel->getColors3d();
		$this->template->t = 'FALSE';
		$this->payload->showdetail = false;

	}


	public function actionDefault()
	{
		$this->template->usage = $this->session->use;
	}

	public function actionExterior()
	{
		$this->act = 'showColor!';
		$this->table = 'e_ccolors2d';
		$this->template->c2d = true;
		$this->template->table = $this->table;
		$this->session->use = self::EXTERIOR;
		$this->template->usage = $this->session->use;
	}

	public function actionInterior()
	{
		$this->act = 'showColor!';
		$this->table = 'e_ccolors2d';
		$this->session->use = self::INTERIOR;
		$this->template->usage = $this->session->use;
		$this->template->preview = $this->session->preview;
	}

	public function actionDetail($id)
	{
		$this->session->use = self::INTERIOR;
		$this->template->usage = $this->session->use;
		$this->template->table = $this->table;

	}

	public function handleShowColor($colorId,$table)
	{
		if(!$this->isAjax()){
		}
		$this->payload->det = true;
		$this->template->colorX = true;
		$this->table = $table;
		$this->color = $this->database->table($table)->where('id = ?',$colorId)->fetch();
		$this->template->color = $this->color;
		$this->redrawControl('detail');
	}


	public function handleTab($table)
	{
		if(!$this->isAjax()){
			$this->redirect('this');
		}
		if($table == 1){
			$this->payload->tt = 1;
			$this->table = 'e_ccolors3d';
		} else {
			$this->payload->tt = 0;
			$this->table = 'e_ccolors2d';
		}
		$this->template->table = $this->table;
		$this->redrawControl('wrapper');

	}

	public function handleSelect($colorId)
	{
		if(!$this->isAjax()){
			$this->redirect('this');
		}
		$this->payload->select = true;
		$this->redrawControl('select');

	}


	public function createComponentColors2d()
	{
		$control = new \Components\ColorsControl($this->database);
		$control->setTable('e_ccolors2d');
		return $control;
	}

	public function createComponentColors3d()
	{
		$control = new \Components\ColorsControl($this->database);
		$control->setTable('e_ccolors3d');
		return $control;
	}

	public function createComponentColorDetail()
	{
		$control = new \Components\ColorDetailControl($this->database);
		$control->setTable($this->table);
		$control->setColor($this->color);
		return $control;
	}

}
