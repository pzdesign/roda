<?php
/**
 * Created by PhpStorm.
 * User: flashk
 * Date: 18.09.2016
 * Time: 23:04
 */

namespace Components;

use Nette\Application\UI\Control;
use Nette\Database\Context;
use Tracy\Debugger;

class ColorDetailControl extends Control
{
	/** @var \Models\ColorsModel */
	protected $model;

	/** @var Context $database */
	protected $database;

	const NAMES = false;

	public $table;

	public $color;

	public function __construct(Context $database)
	{
		$this->database = $database;
	}

	public function setTable($table)
	{
		$this->table = $table;
	}

	public function setColor($color)
	{
		$this->color = $color;
	}

	public function render()
	{
		$template = $this->template;
		$template->setFile(COMPONENTS_TPL_DIR . '/Color.latte');
		// vložíme do šablony nějaké parametry
		$template->color = $this->color;
		$template->names = self::NAMES;
		$template->table = $this->presenter->table;
		$template->parent = $par = $this->color->related($this->table,'parent');
		$template->parents = $this->database->table($this->table)->where('parent = ?',$this->color->parent)->order('hbz ASC');
		$this->redrawControl();
		//Debugger::barDump($this->color);
		Debugger::barDump($par);
		//Debugger::barDump($this->presenter->table);
		$template->render();
	}

}