<?php
/**
 * Created by PhpStorm.
 * User: flashk
 * Date: 18.09.2016
 * Time: 23:04
 */

namespace Components;

use Nette\Application\UI\Control;
use Nette\Database\Context;

class ColorsControl extends Control
{
	/** @var \Models\ColorsModel */
	protected $model;

	/** @var Context $database */
	protected $database;

	const NAMES = false;

	public $table;

	public function __construct(Context $database)
	{
		$this->database = $database;
	}

	public function setTable($table)
	{
		$this->table = $table;
	}

	public function render()
	{
		$template = $this->template;
		$template->setFile(COMPONENTS_TPL_DIR . '/Colors.latte');
		// vložíme do šablony nějaké parametry
		$template->colors = $this->database->table($this->table)->where('active = ?',1);
		$template->names = self::NAMES;
		$template->table = $this->table;
		$template->render();
	}

}