<?php

/**
 * This file is part of Weseus CMS.
 *
 * @author Jan Kočí <skippous@gmail.com>
 * @copyright (c) 2012 Webový servis Company s.r.o 
 * 
 */

namespace Weseus\Forms;

use Nette\Application\UI,
    Nette\ComponentModel\IContainer;

class BasketForm extends BaseForm {

    public $basket;
    protected $items;
    protected $sum;

    public function __construct($parent = NULL, $name = NULL, $basket) {
        parent::__construct($parent, $name);

        $this->basket = $basket;
        $this->items = $this->basket->getItems();
        //  $this->presenter->dd($this->basket, 'basket');

        foreach ($this->items as $item) {

            //product
            if (!empty($item->e_products_id) && !$item->is_gift)
                $this->addText($item->e_products_id)->setDefaultValue($item->amount);

            //service
            if (!empty($item->e_extra_services_id)) {
                $this->addCheckbox('service' . $item->e_extra_services_id);
                //is checked
                if ($item->amount > 0) {
                    $this['service' . $item->e_extra_services_id]->setDefaultValue(true);
                    //   $form['service' . $item->e_extra_services_id]->getControlPrototype()->onclick = "$.get('?do=addService( $item->e_extra_services_id, $item->e_extra_services_id)');";
                }
            }
        }

        $this->addSubmit('send', 'Přepočítat');

        $this->onSuccess[] = callback($this, 'basketFormSubmitted');
    }

    public function render() {
        $this->template->basket = $this->presenter->items;
        $this->template->itemsCount = $this->presenter->itemsCount;
        $this->template->sum = $this->presenter->sum;
        parent::render();
    }

    public function basketFormSubmitted($form) {

        foreach ($this->basket->getItems() as $item) {

            if (!empty($item->e_products_id) && !$item->is_gift)
                $this->basket->setAmount($item->e_products_id, $form[$item->e_products_id]->value);
        }

        $this->items = $this->basket->getItems();

        //$this->presenter->invalidateControl();
        $this->presenter->handleRefresh();
    }

}