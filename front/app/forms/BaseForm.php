<?php

/**
 * This file is part of Weseus CMS.
 *
 * @author Jan Kočí <skippous@gmail.com>
 * @copyright (c) 2012 Webový servis Company s.r.o 
 * 
 */

namespace Weseus\Forms;

use Nette\Application\UI,
    Nette\ComponentModel\IContainer;

class BaseForm extends UI\Form {

    protected $template;
    
    public function __construct(IContainer $parent = NULL, $name = NULL) {
        parent::__construct($parent, $name);

        $templateName = lcfirst($this->name);
        $this->template = $this->presenter->createTemplate();
        $this->template->setFile(FORMS_TPL_DIR . "/$templateName.latte");
        
        $this->setTranslator($this->presenter->context->translator);
    }

    public function render() {
        $this->template->form = $this;
        $this->template->render();
    }

}