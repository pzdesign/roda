<?php
/**
 * Created by PhpStorm.
 * User: flashk
 * Date: 10.09.2016
 * Time: 15:52
 */

namespace MainPresenter;

use Nette;
use WebLoader\Compiler;
use WebLoader\FileCollection;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

abstract class MainPresenter extends Nette\Application\UI\Presenter
{
    /**
     * Továrnička pro komponentu načítající CSS.
     * @return CssLoader
     */
    public function createComponentCss() {
        // připravíme seznam souborů
        // FileCollection v konstruktoru může dostat výchozí adresář, pak není potřeba psát absolutní cesty
        $files = new FileCollection(WWW_DIR . '/css');

        // bootstrap
        $files->addFile('bootstrap.min.css');

        // custom style
        $files->addFile('main.css');

        // kompilátoru seznam předáme a určíme adresář, kam má kompilovat
        $compiler = Compiler::createCssCompiler($files, WWW_DIR . '/webtemp');

        $compiler->addFilter(function ($code) {
            return \CssMin::minify($code);
        });
        // nette komponenta pro výpis <link>ů přijímá kompilátor a cestu k adresáři na webu
        return new CssLoader($compiler, $this->template->basePath . '/webtemp');
    }

    /**
     * Továrnička pro komponentu načítající JS.
     * @return JavaScriptLoader
     */
    public function createComponentJs() {
        $files = new FileCollection(WWW_DIR . '/js');
        // můžeme načíst i externí js
        //$files->addRemoteFile('http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js');
        //jQuery
        $files->addFile('jquery-3.1.0.min.js');
        //jQuery UI
        $files->addFile('bootstrap.min.js');

        //Defaultni JS
        $files->addFile('init.js');


        $compiler = Compiler::createJsCompiler($files, WWW_DIR . '/webtemp');

        //minifikace
        $compiler->addFilter(function ($code) {
            return \JSMin::minify($code);
        });

        return new JavaScriptLoader($compiler, $this->template->basePath . '/webtemp');
    }

}