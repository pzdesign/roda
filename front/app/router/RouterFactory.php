<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;
        // Admin
        $router[] = new Route('admin/<presenter>/<action>/<id>', array(
            'module' => 'Admin',
            'presenter' => 'Main',
            'action' => 'default',
            'id' => NULL,
        ));


        $router[] = new Route('<presenter>/<action>/<id>', array(
            'module' => 'Landing',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));

        return $router;
    }

}
