<?php

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\SimpleRouter;

require __DIR__ . '/../../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

// Setup router using mod_rewrite detection

$router = $container->getByType(Nette\Application\IRouter::class);
$router[] = new Route('index.php', 'Landing:Homepage:default', Route::ONE_WAY);


$router[] = $adminRouter = new RouteList('Admin');
$adminRouter[] = new Route('admin/<presenter>/<action>', 'Homepage:default');

$router[] = $colorRouter = new RouteList('Color');
//$colorRouter[] = new Route('studio/<action>[/<id>]', 'Homepage:default');
//routa

$colorRouter[] = new Route('studio[/<action>][/<id>]', array(
    'presenter' => 'Homepage',
    'action' => array(
        Route::VALUE => 'default',
        Route::FILTER_TABLE => array(
            // řetězec v URL => action
            'exterier' => 'exterior',
            'interier' => 'interior',
        ),
    ),
    'id' => NULL,
));


$router[] = $frontRouter = new RouteList('Landing');
$frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');


return $container;
