<?php
/**
 * Created by PhpStorm.
 * User: webovyservis
 * Date: 14.09.2016
 * Time: 17:59
 */

namespace Models;


use Nette\Database\Context;

class ColorsModel extends BaseModel
{
    /** @var  Context $database
     * */
    protected $database;


    public function getColors2d($amphisilan = false)
    {
        if(!$amphisilan) {
            return $this->database->table('e_ccolors2d')->where('main_color = ? OR main_color = ?',0,1);
        } else {
            return $this->database->table('e_ccolors2d')->where('main_color = ?',2);;
        }
    }

    public function getColors3d()
    {
        return $this->database->table('e_ccolors3d');
    }


    public function getColor($colorId)
    {
        return $this->getColors2d()->where('id = ?',$colorId)->fetch();
    }

	public function getColor3D($colorId)
	{
		return $this->getColors3d()->where('id = ?',$colorId)->fetch();
	}


    public function getColorsType($amphiSilan = true)
    {
        if($amphiSilan == 1) {
            return $this->getColors2d()->where('main_color = ?',1);
        } elseif($amphiSilan) {
            return $this->getColors2d()->where('main_color = ? OR main_color = ?',0,1);
        } else {
            return $this->getColors2d()->where('main_color',2);
        }
    }

    public function getSubcolors($colorId)
    {
        return $this->getColor($colorId);
    }

}