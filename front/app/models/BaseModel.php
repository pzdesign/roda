<?php
/**
 * Created by PhpStorm.
 * User: webovyservis
 * Date: 14.09.2016
 * Time: 17:57
 */

namespace Models;


use Nette\Database\Context;
use Nette\Object;

abstract class BaseModel extends Object
{
    /** @var Context $database */
    protected $database;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }


}