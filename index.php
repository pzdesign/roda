<?php

//session_start();

//$oldAuth = $_SESSION['auth'];

// absolute filesystem path to this web root
define('WWW_DIR', __DIR__);

// absolute filesystem path to this web root
define('FRONT_DIR', __DIR__ . '/front');

// absolute filesystem path to the application root
define('APP_DIR', WWW_DIR . '/front/app');

// absolute filesystem path to the libraries
define('LIBS_DIR', WWW_DIR . '/front/libs');

// absolute filesystem path to the weseus CMS components templates dir
define('COMPONENTS_TPL_DIR', APP_DIR . '/templates/Components');

// absolute filesystem path to product images temp dir
define('PRODUCTS_TMP_DIR', 'webtemp/e_products');

// absolute filesystem path to product images temp dir
define('FORMS_TPL_DIR', APP_DIR . '/templates/Forms');

// absolute filesystem path to old admin
define('ADMIN_DIR', WWW_DIR . '/admin');

// absolute filesystem path to old admin
define('EXPORT_DIR', WWW_DIR . '/export');


$container = require APP_DIR . '/bootstrap.php';

$container->getByType(Nette\Application\Application::class)
	->run();
